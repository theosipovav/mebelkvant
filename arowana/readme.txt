=== Arowana ===

Contributors: Nayrathemes
Requires at least: 4.4
Tested up to: 5.5
Requires PHP: 5.6
Stable tag: 2.0.31
Version: 2.0.31
License: GPLv3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html
Tags: one-column, two-columns, left-sidebar, right-sidebar, flexible-header, custom-background, custom-colors, custom-header, custom-menu,  custom-logo, footer-widgets, full-width-template, sticky-post, theme-options, threaded-comments, translation-ready, blog, entertainment, portfolio, editor-style, grid-layout

== Copyright ==

Arowana WordPress Theme, Copyright 2020 Nayra Themes
Arowana is distributed under the terms of the GPLv2 or later


StartKit WordPress Theme, Copyright 2020 Nayra Themes. StartKit WordPress Theme is distributed under the terms of the GPLv2 or later

== Description ==

Arowana is a child of the StartKit theme with lots of powerful features. Arowana is suited for any type of website and works with all popular plugins. Get 24x7 support with both the free and the Pro versions. Explore the Pro demo - https://www.nayrathemes.com/demo/pro/?theme=arowana

== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Arowana includes support for Infinite Scroll in Jetpack and other following Plugins.
1. Clever Fox

-------------------------------------------------------------------------------------------------
License and Copyrights for Resources used in Arowana WordPress Theme
-------------------------------------------------------------------------------------------------

1) Package Structure
Based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

2) Font Awesome
=================================================================================================
Font Awesome 4.7.0 by @davegandy - http://fontawesome.io - @fontawesome
License - Font: SIL OFL 1.1, CSS: MIT License(http://fontawesome.io/license)
Source: http://fontawesome.io

3) Bootstrap Framework
=================================================================================================
Bootstrap (http://getbootstrap.com/)
Copyright (c) 2011-2019 Twitter, Inc.
License - MIT License (https://github.com/twbs/bootstrap/blob/master/LICENSE)

4) Animate Css
=================================================================================================
Animate Css by @Daniel Eden - https://daneden.me/
License - MIT License (https://github.com/daneden/animate.css/blob/master/LICENSE)
Source: https://github.com/daneden/animate.css

5) Sticky Js
=================================================================================================
Sticky Js by @Anthony Garand - http://stickyjs.com/
License - MIT License (https://github.com/garand/sticky/blob/master/LICENSE.md)
Source: http://stickyjs.com/

6) Meanmenu Js
=================================================================================================
jquery.meanmenu.js by Chris Wharton
License - GNU LESSER GENERAL PUBLIC LICENSE (https://github.com/meanthemes/meanMenu/blob/master/gpl.txt)
Source: http://www.meanthemes.com/plugins/meanmenu/

7) Wow Js
=================================================================================================
jquery.meanmenu.js by Thomas Grainger
License -  MIT license (https://github.com/graingert/WOW/blob/master/LICENSE)
Source: https://github.com/graingert/WOW/blob/master/dist/wow.min.js

8) Sainitization
=================================================================================================
License - GNU General Public License (https://github.com/WPTRT/code-examples/blob/master/LICENSE)
Source: https://github.com/WPTRT/code-examples

9) wp-bootstrap-navwalker
=================================================================================================
License -  GNU GENERAL PUBLIC LICENSE (https://github.com/wp-bootstrap/wp-bootstrap-navwalker/blob/master/LICENSE.txt)
Source: https://github.com/wp-bootstrap/wp-bootstrap-navwalker

10) pure-css-toggle-buttons
=================================================================================================
License -   MIT license (https://github.com/soderlind/class-customizer-toggle-control/blob/master/pure-css-toggle-buttons/license.txt)
Source: https://github.com/soderlind/class-customizer-toggle-control

11) WordPress Customizer Toggle Control
=================================================================================================
License -  GNU GENERAL PUBLIC LICENSE (https://www.gnu.org/licenses/)
Source: https://github.com/soderlind/class-customizer-toggle-control

12) Customizer Repeater Control
=================================================================================================
License -  MIT LICENSE (https://github.com/cristian-ungureanu/customizer-repeater/blob/production/LICENSE)
Source: https://github.com/cristian-ungureanu/customizer-repeater

13) Screenshot Image
=================================================================================================
Screenshot Image
URL: https://pxhere.com/en/photo/1446785
Source: https://pxhere.com
License: CC0 Public Domain

14) Image Folder Images
=================================================================================================
All other Images have been used in images folder, Created by Nayra Themes. Also they are GPL Licensed and free to use and free to redistribute further.


== Changelog ==

@version 2.0.31
* Changed Escaping in Footer Copyright

@version 2.0.30 
* Tested with WordPress 5.5

@version 2.0.29
* Added User Icon in Blog Meta

@version 2.0.28
* Code Improvement in Header

@version 2.0.27
* Code Improvement

@version 2.0.26
* Removed Unnecessary Styles

@version 2.0.25
* Screenshot Updated

@version 2.0.24
* Customizer Repeater Control Licence Added

@version 2.0.23
* Tested With WordPress 5.4.2

@version 2.0.22
 * Code Improvement in Footer
 
@version 2.0.21
 * Improved wp_body_open Function

@version 2.0.20
* Tested up to & Requires PHP Fields Added in Style.css

@version 2.0.19
* Removed Unnecessary Styles

@version 2.0.18
* Removed Unnecessary Styles

@version 2.0.17
* Tested With WordPress 5.4.1 

@version 2.0.16
* Subject Tag Updated

@version 2.0.15
* Code Improvement

@version 2.0.14
* Keyboard Navigation Improved
* Code Improvement

@version 2.0.13
* Tested With WordPress 5.4 

@version 2.0.12
* Code Improvement In Blog Section

@version 2.0.11
* Remove Unnecessary Styles

@version 2.0.10
* Prefixing Issues Resolved

@version 2.0.9
* Remove Unnecessary Styles

@version 2.0.8
* Screenshot Updated

@version 2.0.7
* Menu Active Style Added

@version 2.0.6
* Code Optimize In Navigation File

@version 2.0.5
* Code Optimize In Navigation

@version 2.0.4
* Remove Unnecessary Style

@version 2.0.3
* Remove Unnecessary Style

@version 2.0.2
* Remove Unnecessary Style

@version 2.0.1
* Code Improvement In Footer Section

@version 2.0
* Code Improvement

@version 1.9
* Remove Unnecessary Style

@version 1.8
* Copyright Updated

@version 1.7
* Remove Unnecessary Style

@version 1.6
* Remove Unnecessary Style

@version 1.5
* Tested With WordPress 5.3.2

@version 1.4
* Remove Unnecessary Style

@version 1.3
* Code Improvement

@version 1.2
* Screenshot Updated

@version 1.0.20
* Tested With WordPress 5.3.1
* Style Improvement

@version 1.0.19
* Add Upsale Button

@version 1.0.18
* Site Title Styled

@version 1.0.17
* Screenshot Changed

@version 1.0.16
* Screenshot Changed

@version 1.0.15
* Style Improvement

@version 1.0.14
* Subject Tags Updated

@version 1.0.13
* Resolve Escaping Issue

@version 1.0.12
* Theme Description Changed

@version 1.0.11
* Theme Description Changed

@version 1.0.10
* Remove Unnecessary Style

@version 1.0.9
* Tested With WordPress 5.3

@version 1.0.8
* Screenshot Updated

@version 1.0.7
* Screenshot Updated

@version 1.0.6
* Remove Unnecessary Style

@version 1.0.5
* Remove Unnecessary Style

@version 1.0.4
* Style Improvement

@version 1.0.3
* Theme URI Added

@version 1.0.2
* Theme Review Issues Resolved

@version 1.0.1
* Theme Review Issues Resolved

@version 1.0
* Initial release