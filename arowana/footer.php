<!-- Start: Footer Sidebar
============================= -->
<?php if (is_active_sidebar('footer-widget-area')) { ?>
	<footer id="footer-widgets" class="footer-sidebar">
		<div class="container">
			<div class="row">
				<?php dynamic_sidebar('footer-widget-area'); ?>
			</div>
		</div>
	</footer>
<?php } ?>
<!-- End: Footer Sidebar
============================= -->
<?php
$hide_show_copyright = get_theme_mod('hide_show_copyright', '1');
$copyright_content = get_theme_mod('copyright_content');
?>

<section id="footer-copyright">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="text-center">
					<?php
					if ($hide_show_copyright == '1') {
						if (!empty($copyright_content)) {
					?>
							<p><?php echo wp_kses_post($copyright_content); ?></p>
						<?php
						} else {
						?>
							<p><?php esc_html_e('&copy; 2019 All Right Reserved | Arowana WordPress Theme', 'arowana'); ?></p>
					<?php
						}
					}
					?>
					<a href="#" class="scrollup"><i class="fa fa-arrow-up"></i></a>
				</div>
			</div>
			<div class="col-md-6">
				<?php
				$hide_show_payment = get_theme_mod('hide_show_payment', '1');
				$footer_Payment_icon = get_theme_mod('footer_Payment_icon');
				?>

				<?php if ($hide_show_payment == '1') { ?>
					<ul class="payment-icon">
						<?php
						$footer_Payment_icon = json_decode($footer_Payment_icon);
						if ($footer_Payment_icon != '') {
							foreach ($footer_Payment_icon as $footer_Payment_item) {
								$icon = !empty($footer_Payment_item->icon_value) ? apply_filters('startkit_translate_single_string', $footer_Payment_item->icon_value, 'footer section') : '';
								$icon_link = !empty($footer_Payment_item->link) ? apply_filters('startkit_translate_single_string', $footer_Payment_item->link, 'footer section') : '';
						?>
								<?php if (!empty($icon)) : ?>
									<li><a href="<?php echo esc_url($icon_link); ?>"><i class="fa <?php echo esc_attr($icon); ?>"></i></a></li>
								<?php endif; ?>
						<?php  }
						} ?>
					</ul>
				<?php } ?>
			</div>
		</div>
	</div>
</section>
</div>
</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
	(function(m, e, t, r, i, k, a) {
		m[i] = m[i] || function() {
			(m[i].a = m[i].a || []).push(arguments)
		};
		m[i].l = 1 * new Date();
		k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
	})
	(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

	ym(69070876, "init", {
		clickmap: true,
		trackLinks: true,
		accurateTrackBounce: true,
		webvisor: true
	});
</script>
<noscript>
	<div><img src="https://mc.yandex.ru/watch/69070876" style="position:absolute; left:-9999px;" alt="" /></div>
</noscript>
<!-- /Yandex.Metrika counter -->

<?php wp_footer(); ?>

<style>
	#services .service-icon {
		background: none;
	}

	#services .service-icon:after {
		display: none !important;
	}
</style>

<script>
	jQuery(document).ready(function() {

		jQuery("#services .service-icon").addClass("q202011100008");
		var section = jQuery("#services .service-icon");

		var s1 = jQuery(section[0]).html('<img src="/wp-content/uploads/2020/11/logo-byurokrat.png" alt="">');
		var s1 = jQuery(section[1]).html('<img src="/wp-content/uploads/2020/11/logo-mireii.png" alt="">');
		var s1 = jQuery(section[2]).html('<img src="/wp-content/uploads/2020/11/logo-fabrikanty.png" alt="">');
		var s1 = jQuery(section[3]).html('<img src="/wp-content/uploads/2020/11/logo-metos.png" alt="">');


	});
</script>
</body>

</html>